"use client";
import PropTypes from "prop-types";
import React, { useState } from "react";
import Image from "next/image";

const Weather = ({ data }) => {
  console.log(data);


  return (
    <div className="relative flex flex-col justify-between max-w-[800px] w-full h-[90vh] m-auto p-4 text-gray-300 z-[1]">
      {data.weather && (
        <div className="relative flex flex-col gap-36 justify-between pt-12">
          <div className="flex justify-between items-center">
          <div className="flex flex-col items-center ">
            <Image
              src={`http://openweathermap.org/img/wn/${data.weather[0].icon}@4x.png`}
              alt="sky"
              width={100}
              height={100}
            />
            <p className="text-2xl text-black/75">{data.weather[0].main}</p>
          </div>
          <p className="text-9xl text-black/70">{data.main.temp.toFixed(0)}&#176;</p>
          </div>
          <div className="bg-black/50 relative p-8 rounded-md">
            <p className="text-2xl text-center pb-6">Weather in {data.name}</p>
            <div className="flex justify-between text-center">
              <div>
                <p className="font-bold text-2xl">{data.main.feels_like.toFixed(0)}&#176;</p>
                <p className="text-xl">Feels Like</p>
              </div>
              <div>
                <p className="font-bold text-2xl">{data.main.humidity}%</p>
                <p className="text-xl">Humidity</p>
              </div>
              <div>
                <p className="font-bold text-2xl">{data.wind.speed.toFixed(0)} MPH</p>
                <p className="text-xl">Wind</p>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
Weather.propTypes = {
  data: PropTypes.shape({
    weather: PropTypes.arrayOf(
      PropTypes.shape({
        icon: PropTypes.string.isRequired,
        main: PropTypes.string.isRequired,
      })
    ).isRequired,
    main: PropTypes.arrayOf(
      PropTypes.shape({
        temp: PropTypes.string.isRequired,
      })
    ),
  }).isRequired,
};
export default Weather;
