import Image from 'next/image'
import React from 'react'
import spinner from '../../public/gifs/Spinner.gif'
const Spinner = () => {
  return (
    <>
    <Image className='w-[100px] m-auto block' src={spinner} alt='loading....'/>
    </>
  )
}

export default Spinner