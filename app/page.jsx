"use client";
import Image from "next/image";
import bgImg from "../public/images/bggg.jpg";
import Spinner from "./components/Spinner";
import { useState } from "react";
import axios from "axios";
import search from "../public/icons/search.svg";
import Weather from "./components/Weather";

export default function Home() {
  const [city, setCity] = useState("");
  const [weather, setWeather] = useState({});
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  const url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${process.env.NEXT_PUBLIC_WEATHER_KEY}`;

  const fetchWeather = (e) => {
    e.preventDefault();
    setWeather("");
    setError(false);
    setLoading(true);
    axios
      .get(url)
      .then((res) => {
        setWeather(res.data);
        console.log(res.data);
      })
      .catch((err) => {
        console.log(err);
        setError(true);
      })
      .finally(() => {
        setCity("");
        setLoading(false);
      });
  };
  if (loading) {
    return <Spinner />;
  } else {
    return (
      <main>
        <div className="absolute top-0 left-0 right-0 bottom-0 bg-black/40 z-10">
          <Image
            src={bgImg}
            alt="weather app bg-image"
            layout="fill"
            className="object-cover"
          />
        </div>
        <div className="relative flex flex-col mt-8 justify-center items-center max-w-[500px] w-full m-auto pt-4 z-10">
          <form
            onSubmit={fetchWeather}
            className="flex justify-between items-center w-full m-auto p-3 bg-black/10 border border-gray-600 text-black rounded-2xl"
          >
            <div>
              <input
                onChange={(e) => setCity(e.target.value)}
                className="bg-transparent border-none placeholder-black/65 text-black focus:outline-none text-xl"
                type="text"
                placeholder="Enter city"
              />
            </div>
            <button>
              <Image src={search} alt="search icon" width={28} height={28} />
            </button>
          </form>
          {error && (
            <div className="mt-24 bg-black/40 p-10 rounded-md text-2xl">
              <p className=" text-red-300 text-center ">City not found</p>
            </div>
          )}
          <div className="w-full">
            <Weather data={weather} />
          </div>
        </div>
      </main>
    );
  }
}
